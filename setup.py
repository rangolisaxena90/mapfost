from setuptools import setup

setup(name='mapfost',
      version='4.2.5',
      description='Implementation of the autofocus method MAPFoSt.'
                  ' Publication: Binding J, Mikula S, Denk W. Low-dosage Maximum-A-Posteriori Focusing and Stigmation. Microsc Microanal. 2013',
      url='https://github.com/pypa/mapfost',
      author='Rangoli Saxena',
      author_email='rangolisaxena90@gmail.com',
      license='MIT',
      packages=['mapfost'],
      zip_safe=False)